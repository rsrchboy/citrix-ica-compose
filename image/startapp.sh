#!/bin/sh

# wait for the other service to become available
/wait-for-it.sh ${ICA_HOST:-icaclient}:22 -t ${WAIT_TIMEOUT:-300}

ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -Y app@${ICA_HOST:-icaclient}
