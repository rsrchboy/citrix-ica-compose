tl;dr:

    $ docker-compose up -d
    $ xdg-open http://localhost:15800

# ICA in a box!

...because we don't want it all over the place, really.

There are a number of excellent containers out there that have already done
the hard work of packaging firefox, `icaclient`, etc, but (naturally) note
that did exactly what I wanted.

I've long been a fan of the
[jlesage/firefox](https://hub.docker.com/r/jlesage/firefox) image, for the
neat way it keeps all the messy bits of running firefox inside a container.
Running an X11 server that displays via VNC or an embedded vnc-over-http is
brilliant.

Similarly,
[desktopcontainers/icaclient](https://hub.docker.com/r/desktopcontainers/icaclient)
bundles up a browser and the `icaclient` install, then displays mozilla on
your desktop automatically when you `ssh -X` into the container.  Very nice.

However — and this may be just me — I didn't want ICA having access to my
laptop's X11.  Trying to take the second image and extend the first proved to
be awkward, as `icaclient` doesn't really know how to deal with alpinelinux.

So.  Enter `docker-compose`.

# Run `desktopcontainers/icaclient`, access via `jlesage/baseimage-gui`

This compose configuration:

1. Launches an `icaclient` service
   [desktopcontainers/icaclient](https://hub.docker.com/r/desktopcontainers/icaclient)
1. Builds an image based off of
   [jlesage/baseimage-gui](https://hub.docker.com/r/jlesage/baseimage-gui)
   that waits for `icaclient:22` to become available, then connects via ssh to
   launch mozilla/etc

Once brought up, you can connect by navigating to http://localhost:15800 or
vnc://localhost:15900.

# Configuration

There are a number of environment variables this compose configuration will
respond to, e.g.:

| var           | default    | description                                         |
|---------------|------------|-----------------------------------------------------|
| `WEB_URL`       | ""         | Initial URL to launch; e.g. your citrix launch-page |
| `ICA_BIND_HOST` | 127.0.0.1  | Where to bind our forwarded ports                   |
| `ICA_HTTP_PORT` | 15800      | Port to bind for HTTP access                        |
| `ICA_VNC_PORT`  | 15900      | Port to bind for VNC access                         |
| `ICA_SSH_PORT`  | \<random\> | Port to bind for ssh access                         |

These can all be set either in the shell environment, or in a
[`.env`](https://docs.docker.com/compose/env-file/) file.
